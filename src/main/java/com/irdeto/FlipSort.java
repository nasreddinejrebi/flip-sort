package com.irdeto;

import java.util.Arrays;

public class FlipSort {

    public int[] flipSort(int[] elements) throws InvalidArrayExecption {
        checkArrayLength(elements);
        checkArrayElements(elements);
        checkDuplicateElements(elements);
        int maxNumberOfSequence = 10 * elements.length;
        int[] flipSequences = new int[maxNumberOfSequence];
        int flipNumber = 0;
        int arrayLength = elements.length;

        // Initialisation : Flip All the table
        int k = arrayLength - 1;
        flipSequences[flipNumber++] = k + 1;
        reverseSubArray(elements, k);

        while (flipNumber < maxNumberOfSequence) {
            //Search the greatest value
            int maxIndex = getMaxIndex(elements, arrayLength);

            // if greatest value in index 0 we flip all the table and we decrease the array size by 1
            // or we flip sub table to max index
            if (maxIndex == 0) {
                arrayLength -= 1;
                k = arrayLength;
            } else {
                k = maxIndex;
            }

            //save flip index
            flipSequences[flipNumber++] = k + 1;
            // reverse sub array from 0 to k
            reverseSubArray(elements, k);
            // check if the table is sorted
            if (isSorted(elements)) {
                return Arrays.copyOf(flipSequences, flipNumber);
            }
        }
        throw new InvalidArrayExecption("Array can't be sorted");
    }

    private int getMaxIndex(int[] elements, int arrayLength) {
        int max = 0;
        for (int i = 1; i < arrayLength; i++) {
            if (elements[i] > elements[max]) {
                max = i;
            }
        }
        return max;
    }

    private void reverseSubArray(int[] elements, int reverseIndex) {
        for (int i = 0; i < (reverseIndex + 1) / 2; i++) {
            int tempswap = elements[i];
            elements[i] = elements[reverseIndex - i];
            elements[reverseIndex - i] = tempswap;
        }
    }

    private boolean isSorted(int[] elements) {
        for (int i = 0; i < elements.length - 1; i++) {
            if (elements[i] > elements[i + 1]) {
                return false;
            }
        }

        return true;
    }

    private void checkDuplicateElements(int[] elements) throws InvalidArrayExecption {
        for (int i = 0; i < elements.length; i++) {
            for (int j = i + 1; j < elements.length; j++) {
                if (elements[i] == elements[j]) {
                    throw new InvalidArrayExecption("All elements in the array must be unique");
                }
            }
        }
    }

    private void checkArrayElements(int[] elements) throws InvalidArrayExecption {
        for (int element : elements) {
            if (element < 1 || element > elements.length) {
                throw new InvalidArrayExecption("Array values must be between 1 and " + elements.length);
            }
        }
    }

    private void checkArrayLength(int[] elements) throws InvalidArrayExecption {
        if (elements.length < 1 || elements.length > 100) {
            throw new InvalidArrayExecption("array length must be between 1 and 100");
        }
    }

}
