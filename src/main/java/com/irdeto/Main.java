package com.irdeto;

import java.util.Scanner;
import java.util.StringJoiner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] elements = getUserArray(scanner);
        try {
            int[] flipSequences = new FlipSort().flipSort(elements);
            System.out.print("We flipped the array " + flipSequences.length + " times with the indexes: ");
            System.out.println(formatSequenceArray(flipSequences));
        } catch (InvalidArrayExecption invalidArrayExecption) {
            System.out.println(invalidArrayExecption.getMessage());
        }
    }

    private static int[] getUserArray(Scanner scanner) {
        System.out.print("Enter no. of elements you want in array:");
        int numberOfElement = scanner.nextInt();
        int[] elements = new int[numberOfElement];
        for (int i = 0; i < numberOfElement; i++) {
            System.out.print("Enter element [" + (i + 1) + "]:");
            elements[i] = scanner.nextInt();
        }
        return elements;
    }

    public static String formatSequenceArray(int[] flipSequences) {
        StringJoiner joiner = new StringJoiner(", ");
        for (int flipIndex : flipSequences) {
            joiner.add(String.valueOf(flipIndex));
        }
        return joiner.toString();
    }

}
