package com.irdeto;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class FlipSortTest {

    @Test
    public void validArrayShouldReturnValidFlipSequences() throws InvalidArrayExecption {
        FlipSort sort = new FlipSort();
        int[] arr = {3, 2, 4, 1};

        int[] actualSequences = sort.flipSort(arr);

        int[] expectedSequences ={4, 2, 4, 3};
        assertThat(actualSequences, equalTo(expectedSequences));
    }

    @Test
    public void sortedArrayShouldReturnTwoElementEqualsToArrayLength() throws InvalidArrayExecption {
        FlipSort sort = new FlipSort();
        int[] arr = {1, 2, 3, 4};

        int[] actualSequences = sort.flipSort(arr);

        int[] expectedSequences ={4,4};
        assertThat(actualSequences, equalTo(expectedSequences));
    }

    @Test
    public void arrayContainsNegativeElementsShouldReturnInvalidExceptionWithSpecificMessage() {
        FlipSort sort = new FlipSort();
        int[] arr = {3, 2, -4, 1};

        try {
            sort.flipSort(arr);
            fail("Flip Sort Must throw invalid Array Exception");
        } catch (InvalidArrayExecption ex) {
            assertEquals(ex.getMessage(), "Array values must be between 1 and " + arr.length);
        }
    }

    @Test
    public void arrayContainsElementsGreaterThanTheLengthOfArrayShouldReturnInvalidExceptionWithSpecificMessage() {
        FlipSort sort = new FlipSort();
        int[] arr = {3, 2, 5, 1};

        try {
            sort.flipSort(arr);
            fail("Flip Sort Must throw invalid Array Exception");
        } catch (InvalidArrayExecption ex) {
            assertEquals(ex.getMessage(), "Array values must be between 1 and " + arr.length);
        }
    }

    @Test
    public void arrayLengthGreaterThan100ElementsShouldReturnInvalidExceptionWithSpecificMessage() {
        FlipSort sort = new FlipSort();
        int[] arr = new int[101];

        try {
            sort.flipSort(arr);
            fail("Flip Sort Must throw invalid Array Exception");
        } catch (InvalidArrayExecption ex) {
            assertEquals(ex.getMessage(), "array length must be between 1 and 100");
        }
    }

    @Test
    public void emptyArrayShouldReturnInvalidExceptionWithSpecificMessage() {
        FlipSort sort = new FlipSort();
        int[] arr = {};

        try {
            sort.flipSort(arr);
            fail("Flip Sort Must throw invalid Array Exception");
        } catch (InvalidArrayExecption ex) {
            assertEquals(ex.getMessage(), "array length must be between 1 and 100");
        }
    }

}
